package telecom_number_parser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Main {
	
	private static File createOutputFile(String path) throws IOException {
		File f = new File(path);
		
		if (f.exists()) {
			f.delete();
		}
		
		f.createNewFile();
		return f;
	}
	
	private static void writeFile(List<String> list) throws IOException {
		File fout = createOutputFile("./output.txt");
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	 
		for (String s : list) {
			bw.write(s);
			bw.newLine();			
		}
		
		bw.close();		
	}
	
	private static ActionListener getActionListener(final JTextField countryPrefix, final JTextField numberPrefix, final JLabel confirmationLabel) {
		return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	List<String> list = processInput(countryPrefix, numberPrefix);
                try {
					writeFile(list);
				} catch (IOException e1) {
					confirmationLabel.setText("Error!");
					e1.printStackTrace();
				}
                
                confirmationLabel.setText("Done!");
            }

        };
	}
	
	private static List<String> processInput(JTextField countryPrefix, JTextField numberPrefix) {
		List<String> list = new ArrayList<String>();
		
		if (!isNumeric(countryPrefix.getText()) || !isNumeric(numberPrefix.getText())) {
			list.add("Error: Invalid inputs!");
			return list;
		}
		
		String input = countryPrefix.getText() + numberPrefix.getText();
		list.add(input);
        generateNumbers(list, countryPrefix.getText(), input);
        Collections.sort(list, getComparator());
        
		return list;
	}
	
	private static Comparator<String> getComparator(){
		return new Comparator<String>() {
			
			@Override
			public int compare(String o1, String o2) {
				return _compare(o1,o2);
			}
		};
	}
	
	private static int _compare(String o1, String o2){
		int size = o1.length() <= o2.length() ? o1.length() : o2.length();
		for (int i=0; i<size; i++) {
	        if (Integer.parseInt(String.valueOf(o1.charAt(i))) > Integer.parseInt(String.valueOf(o2.charAt(i)))) {
	        	return 1;
	        } else if (Integer.parseInt(String.valueOf(o1.charAt(i))) < Integer.parseInt(String.valueOf(o2.charAt(i)))) {
	        	return -1;
	        }
	    }
	    return 0;
	}
	
	private static boolean isNumeric(String str) {
	    for (char c : str.toCharArray()) {
	        if (!Character.isDigit(c)) {
	        	return false;
	        }
	    }
	    return true;
	}
	
	private static void generateNumbers(List<String> list, String countryPrefix, String input) {
		if (input.length() == countryPrefix.length()) {
			return;
		}
		
		String lastDigit = input.substring(input.length()-1);
		input = input.substring(0, input.length()-1);
		for (int i=0; i<=9; i++) {
			if (String.valueOf(i).equals(lastDigit)) {
				continue;
			}
			list.add(input + i);
		}
		
		generateNumbers(list, countryPrefix, input);
	}
	
	private static void addWindowElements(JFrame frame) {
		JPanel mainPanel = new JPanel();
		
		JPanel inputsPanel = new JPanel();
		
		JLabel countryPrefixLabel = new JLabel("Contry prefix:");
		JTextField countryPrefix = new JTextField(10);
		JLabel numberPrefixLabel = new JLabel("Number prefix:");
		JTextField numberPrefix = new JTextField(10);
		
		inputsPanel.add(countryPrefixLabel);
		inputsPanel.add(countryPrefix);
		
		inputsPanel.add(numberPrefixLabel);
		inputsPanel.add(numberPrefix);
		
		JPanel confirmationPanel = new JPanel();
        JLabel confirmationLabel = new JLabel("Press 'Generate numbers' button to proceed");
        confirmationPanel.add(confirmationLabel);
		
		JPanel buttonPanel = new JPanel();
		
		JButton okButton = new JButton("Generate numbers");
        ActionListener actionListener = getActionListener(countryPrefix, numberPrefix, confirmationLabel);
        okButton.addActionListener(actionListener);
        
        buttonPanel.add(okButton);
        
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        mainPanel.add(inputsPanel);
        mainPanel.add(Box.createVerticalStrut(10));
		mainPanel.add(buttonPanel);
		mainPanel.add(confirmationPanel);
		
		frame.add(mainPanel);
		
	}

	public static void createWindow() {
		JFrame frame = new JFrame("Telecom - Generate numbers");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		addWindowElements(frame);
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		createWindow();
	}

}